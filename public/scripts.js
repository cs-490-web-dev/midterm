let formName = "midtermForm";

const validateForm = () => {
    validateUser();
    validateEmail();
    validatePosition();
    validateType();
}

const validateType = () => {
    let fullTimeField = document.forms[formName]["full-time"];
    let partTimeField = document.forms[formName]["part-time"];
    let fullTime = fullTimeField.checked;
    let partTime = partTimeField.checked;

    if (fullTime == false & partTime == false) {
        showErrorMessage(null, "type-checkmark", "type-error-message");
    }
    else {
        hideErrorMessage(null, "type-checkmark", "type-error-message");
    }
}

const validatePosition = () => {
    let positionField = document.forms[formName]["position"];
    let position = positionField.value;

    if (position == "not-selected") {
        showErrorMessage(positionField, "position-checkmark", "position-error-message");
    }
    else {
        hideErrorMessage(positionField, "position-checkmark", "position-error-message");
    }
}

const validateEmail = () => {
    let emailField = document.forms[formName]["email"];
    let email = emailField.value;
    let expr = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (!expr.test(email)) {
        showErrorMessage(emailField, "email-checkmark", "email-error-message");
    }
    else {
        hideErrorMessage(emailField, "email-checkmark", "email-error-message");
    }
}

const validateUser = () => {
    let usernameField = document.forms[formName]["username"];
    let username = usernameField.value;
    var expr = /^[a-zA-Z_]{6,10}$/;

    if (!expr.test(username)) {
        showErrorMessage(usernameField, "username-checkmark", "username-error-message")
    }
    else {
        hideErrorMessage(usernameField, "username-checkmark", "username-error-message")
    }
}

const showErrorMessage = (field, checkmark, errorMessage) => {
    document.getElementById(checkmark).style.opacity = "0.0";
    document.getElementById(errorMessage).style.opacity = "1.0";
    
    if (field != null) {
        field.style.borderColor = "red";
    }
}

const hideErrorMessage = (field, checkmark, errorMessage) => {
    document.getElementById(checkmark).style.opacity = "1.0";
    document.getElementById(errorMessage).style.opacity = "0.0";

    if (field != null) {
        field.style.border = "";
    }
}